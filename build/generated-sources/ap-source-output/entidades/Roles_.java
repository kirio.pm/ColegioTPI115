package entidades;

import entidades.Usuarios;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2017-01-19T06:39:29")
@StaticMetamodel(Roles.class)
public class Roles_ { 

    public static volatile SingularAttribute<Roles, Integer> rolId;
    public static volatile SingularAttribute<Roles, String> rolDescripcion;
    public static volatile SingularAttribute<Roles, String> rolNombre;
    public static volatile ListAttribute<Roles, Usuarios> usuariosList;

}