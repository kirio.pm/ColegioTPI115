package entidades;

import entidades.Boleta;
import entidades.Grado;
import entidades.Usuarios;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2017-01-19T06:39:29")
@StaticMetamodel(Alumno.class)
public class Alumno_ { 

    public static volatile SingularAttribute<Alumno, String> alumnoNombre;
    public static volatile SingularAttribute<Alumno, String> alumnoDireccion;
    public static volatile SingularAttribute<Alumno, Grado> gradoId;
    public static volatile SingularAttribute<Alumno, Date> alumnoFecha;
    public static volatile SingularAttribute<Alumno, String> alumnoTelefono;
    public static volatile SingularAttribute<Alumno, Integer> alumnoId;
    public static volatile ListAttribute<Alumno, Boleta> boletaList;
    public static volatile SingularAttribute<Alumno, String> alumnoApellido;
    public static volatile SingularAttribute<Alumno, String> alumnoCarnet;
    public static volatile ListAttribute<Alumno, Usuarios> usuariosList;
    public static volatile SingularAttribute<Alumno, Long> alumnoNie;

}