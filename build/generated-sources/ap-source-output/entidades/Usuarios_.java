package entidades;

import entidades.Alumno;
import entidades.Profesor;
import entidades.Roles;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2017-01-19T06:39:29")
@StaticMetamodel(Usuarios.class)
public class Usuarios_ { 

    public static volatile SingularAttribute<Usuarios, String> usuarioUsuario;
    public static volatile SingularAttribute<Usuarios, String> usuarioNombre;
    public static volatile SingularAttribute<Usuarios, String> usuarioContrasenia;
    public static volatile SingularAttribute<Usuarios, Boolean> usuarioEstado;
    public static volatile SingularAttribute<Usuarios, Roles> rolId;
    public static volatile SingularAttribute<Usuarios, Profesor> profesorId;
    public static volatile SingularAttribute<Usuarios, Alumno> alumnoId;

}