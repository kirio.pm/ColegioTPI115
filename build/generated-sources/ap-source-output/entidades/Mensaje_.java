package entidades;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2017-01-19T06:39:29")
@StaticMetamodel(Mensaje.class)
public class Mensaje_ { 

    public static volatile SingularAttribute<Mensaje, String> mensajeMensaje;
    public static volatile SingularAttribute<Mensaje, Date> mensajeFecha;
    public static volatile SingularAttribute<Mensaje, Integer> mensajeId;
    public static volatile SingularAttribute<Mensaje, String> mensajeNombre;
    public static volatile SingularAttribute<Mensaje, String> mensajeCorreo;

}