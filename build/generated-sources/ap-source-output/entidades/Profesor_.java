package entidades;

import entidades.Grado;
import entidades.Usuarios;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2017-01-19T06:39:29")
@StaticMetamodel(Profesor.class)
public class Profesor_ { 

    public static volatile SingularAttribute<Profesor, String> profesorCarnet;
    public static volatile SingularAttribute<Profesor, String> profesorTelefono;
    public static volatile SingularAttribute<Profesor, Grado> gradoId;
    public static volatile SingularAttribute<Profesor, String> profesorNombre;
    public static volatile SingularAttribute<Profesor, Date> profesorFecha;
    public static volatile SingularAttribute<Profesor, Integer> profesorId;
    public static volatile ListAttribute<Profesor, Usuarios> usuariosList;
    public static volatile SingularAttribute<Profesor, String> profesorApellido;
    public static volatile SingularAttribute<Profesor, String> profesorDireccion;

}