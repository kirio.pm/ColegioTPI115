package entidades;

import entidades.Alumno;
import entidades.Materia;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2017-01-19T06:39:29")
@StaticMetamodel(Boleta.class)
public class Boleta_ { 

    public static volatile SingularAttribute<Boleta, Double> nota1;
    public static volatile SingularAttribute<Boleta, Materia> materiaId;
    public static volatile SingularAttribute<Boleta, Double> nota2;
    public static volatile SingularAttribute<Boleta, Double> nota3;
    public static volatile SingularAttribute<Boleta, Double> nota4;
    public static volatile SingularAttribute<Boleta, Double> nota5;
    public static volatile SingularAttribute<Boleta, Double> nota6;
    public static volatile SingularAttribute<Boleta, Alumno> alumnoId;
    public static volatile SingularAttribute<Boleta, Double> nota7;
    public static volatile SingularAttribute<Boleta, Double> nota8;
    public static volatile SingularAttribute<Boleta, Double> nota9;
    public static volatile SingularAttribute<Boleta, Integer> boletaId;

}