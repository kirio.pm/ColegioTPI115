package entidades;

import entidades.Alumno;
import entidades.Profesor;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2017-01-19T06:39:29")
@StaticMetamodel(Grado.class)
public class Grado_ { 

    public static volatile ListAttribute<Grado, Profesor> profesorList;
    public static volatile SingularAttribute<Grado, Integer> gradoId;
    public static volatile SingularAttribute<Grado, String> gradoNombre;
    public static volatile ListAttribute<Grado, Alumno> alumnoList;
    public static volatile SingularAttribute<Grado, String> gradoDescripcion;

}