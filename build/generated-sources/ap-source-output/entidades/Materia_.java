package entidades;

import entidades.Boleta;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2017-01-19T06:39:29")
@StaticMetamodel(Materia.class)
public class Materia_ { 

    public static volatile SingularAttribute<Materia, Integer> materiaId;
    public static volatile ListAttribute<Materia, Boleta> boletaList;
    public static volatile SingularAttribute<Materia, String> materiaNombre;
    public static volatile SingularAttribute<Materia, String> materiaDescripcion;

}