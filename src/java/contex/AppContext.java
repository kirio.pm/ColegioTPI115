package contex;

import org.springframework.context.ApplicationContext;

//@autor Juanfran Aldana
public class AppContext {

    private static ApplicationContext ctx;

    //Inyectado desde la clase "ApplicationContextProvider" que es automáticamente cargado durante la inicialización de Spring.
    public static void setApplicationContext(ApplicationContext applicationContext) {
        ctx = applicationContext;
    }

    //Obtener acceso al contexto de aplicación de Primavera de todas partes en su aplicación.
    public static ApplicationContext getApplicationContext() {
        return ctx;
    }

    public static Object getBeanSpring(String clase) {
        return AppContext.getApplicationContext().getBean(clase);
    }
   
} 