package contex;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

//Esta clase proporciona un acceso en toda la aplicación al Application Context Spring!
//El contexto de aplicación se inyecta en un método estático de la clase "AppContext"
//Utilice AppContext.getApplicationContext() para obtener acceso a todas las funciones de Spring.

//@autor Juanfran Aldana
public class ApplicationContextProvider implements ApplicationContextAware {

    public void setApplicationContext(ApplicationContext ctx) throws BeansException {
        // Wiring the ApplicationContext into a static method
        AppContext.setApplicationContext(ctx);
    }
} // .EOF
