package dao;

import entidades.Alumno;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

//@autor Juanfran Aldana
@Stateless
public class AlumnoFacade extends AbstractFacade<Alumno> {

    @PersistenceContext(unitName = "colegioPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AlumnoFacade() {
        super(Alumno.class);
    }
    
    //Método que retorna el Alumno por Id. (nuevo_usuario)
    public Alumno alumnoPorId(int alumnoId) {
        try {
            return (Alumno) getEntityManager().createNamedQuery("Alumno.findByAlumnoId").setParameter("alumnoId", alumnoId).getSingleResult();
        } catch (NoResultException e) { //Por si no encuentra ningun resultado que retorne nulo
            return null;
        }
    }
    
    public List<Alumno> alumnosPorGrado(int grado) {
        return getEntityManager().createNamedQuery("Alumno.AlumnosPorGrado").setParameter("grado", grado).getResultList();
    }
    
}
