package dao;

import entidades.Boleta;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

//@autor Juanfran Aldana
@Stateless
public class BoletaFacade extends AbstractFacade<Boleta> {

    @PersistenceContext(unitName = "colegioPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public BoletaFacade() {
        super(Boleta.class);
    }
    
    //Método que retorna la Boleta. (consultarnotaskinder)
    public Boleta traeBoleta(int alumnoId, int materiaId) {
        try {
            return (Boleta) getEntityManager().createNamedQuery("Boleta.Notas").setParameter("alumnoId", alumnoId).setParameter("materiaId", materiaId).getSingleResult();
        } catch (NoResultException e) { //Por si no encuentra ningun resultado que retorne nulo
            return null;
        }
    }
    
}
