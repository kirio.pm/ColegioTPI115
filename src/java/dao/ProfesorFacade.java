package dao;

import entidades.Profesor;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

//@autor Juanfran Aldana
@Stateless
public class ProfesorFacade extends AbstractFacade<Profesor> {

    @PersistenceContext(unitName = "colegioPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ProfesorFacade() {
        super(Profesor.class);
    }
    
    //Método que retorna el Profesor por id. (nuevo_usuario)
    public Profesor profesorPorId(int profesorId) {
        try {
            return (Profesor) getEntityManager().createNamedQuery("Profesor.findByProfesorId").setParameter("profesorId", profesorId).getSingleResult();
        } catch (NoResultException e) { //Por si no encuentra ningun resultado que retorne nulo
            return null;
        }
    }
    
}
