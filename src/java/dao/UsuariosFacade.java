package dao;

import entidades.Usuarios;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

//@autor Juanfran Aldana
@Stateless
public class UsuariosFacade extends AbstractFacade<Usuarios> {

    @PersistenceContext(unitName = "colegioPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UsuariosFacade() {
        super(Usuarios.class);
    }
    
    //Método que retorna el Usuario logueado. (login)
    public Usuarios traeUsuarioLogueado(String nombre) {
        try {
            return (Usuarios) getEntityManager().createNamedQuery("Usuarios.findByUsuarioUsuario").setParameter("usuarioUsuario", nombre).getSingleResult();
        } catch (NoResultException e) { //Por si no encuentra ningun resultado que retorne nulo
            return null;
        }
    }
    
    //Método que retorna el Usuario por Id de profesor. (ver_profesores)
    public Usuarios traeUsuarioPorProfesorId(int profesorId) {
        try {
            return (Usuarios) getEntityManager().createNamedQuery("Usuarios.findByUsuarioPorProfesorId").setParameter("profesorId", profesorId).getSingleResult();
        } catch (NoResultException e) { //Por si no encuentra ningun resultado que retorne nulo
            return null;
        }
    }
    
    //Método que retorna el Usuario por Id de alumno. (ver_alumnos)
    public Usuarios traeUsuarioPorAlumnoId(int alumnoId) {
        try {
            return (Usuarios) getEntityManager().createNamedQuery("Usuarios.findByUsuarioPorAlumnoId").setParameter("alumnoId", alumnoId).getSingleResult();
        } catch (NoResultException e) { //Por si no encuentra ningun resultado que retorne nulo
            return null;
        }
    }
    
}
