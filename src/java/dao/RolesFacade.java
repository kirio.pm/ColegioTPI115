package dao;

import entidades.Roles;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

//@autor Juanfran Aldana
@Stateless
public class RolesFacade extends AbstractFacade<Roles> {

    @PersistenceContext(unitName = "colegioPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public RolesFacade() {
        super(Roles.class);
    }
    
    public List<Roles> rolesNoDirector() {
        return getEntityManager().createNamedQuery("Roles.RolesNoDirector").getResultList();
    }
    
}
