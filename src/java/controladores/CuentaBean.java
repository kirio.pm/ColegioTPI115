package controladores;

import dao.UsuariosFacade;
import entidades.Usuarios;
import java.io.IOException;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

//@author Juanfran Aldana
@ManagedBean
@ViewScoped
public class CuentaBean implements Serializable {

    @EJB
    private UsuariosFacade usuariosFacade;
    private Usuarios usuarioSeleccionado = new Usuarios();

//****************************************************************************//
//                                  Variables                                 //
//****************************************************************************//

    private boolean banderaCorreo = true; //Bandera para habilitar edición de correo. (consultar_cuenta)
    private boolean banderaTelefono = true; //Bandera para habilitar edición de teléfono. (consultar_cuenta)
    private boolean banderaBoton = false;  //Bandera para habilitar botón de guardar edición. (consultar_cuenta)
    
    private String contraseniaActual; //Para guardar la contraseña actual. (cambiar_contrasenia)
    private String contraseniaNueva1; //Para guardar la nueva contraseña. (cambiar_contrasenia)
    private String contraseniaNueva2; //Para validar la nueva contraseña. (cambiar_contrasenia)
    
    private boolean banderaPanel1 = true; //Para mostra/ocultar panel para contraseña actual. (cambiar_contrasenia)
    private boolean banderaPanel2 = false; //Para mostra/ocultar panel para nueva contraseña. (cambiar_contrasenia)
    private boolean banderaPanel3 = false; //Para mostra/ocultar panel para repetir nueva contraseña. (cambiar_contrasenia)
    private boolean banderaPanel4 = false; //Para mostra/ocultar panel para confirmación de contraseña actualizada. (cambiar_contrasenia)
    
    //Session
    @ManagedProperty(value = "#{appSession}")
    private AppSession appSession;
    
    public CuentaBean() {
    }

//****************************************************************************//
//                      Métodos GET y SET para variables                      //
//****************************************************************************//

    public UsuariosFacade getUsuariosFacade() {
        return usuariosFacade;}

    public Usuarios getUsuarioSeleccionado() {
        usuarioSeleccionado = appSession.getUsuario();
        return usuarioSeleccionado;
    }
    public void setUsuarioSeleccionado(Usuarios usuarioSeleccionado) {
        this.usuarioSeleccionado = usuarioSeleccionado;
    }

    public AppSession getAppSession() {
        return appSession;
    }
    public void setAppSession(AppSession appSession) {
        this.appSession = appSession;
    }

    public boolean isBanderaCorreo() {
        return banderaCorreo;
    }
    public void setBanderaCorreo(boolean banderaCorreo) {
        this.banderaCorreo = banderaCorreo;
    }

    public boolean isBanderaTelefono() {
        return banderaTelefono;
    }
    public void setBanderaTelefono(boolean banderaTelefono) {
        this.banderaTelefono = banderaTelefono;
    }

    public boolean isBanderaBoton() {
        return banderaBoton;
    }
    public void setBanderaBoton(boolean banderaBoton) {
        this.banderaBoton = banderaBoton;
    }

    public String getContraseniaActual() {
        return contraseniaActual;
    }
    public void setContraseniaActual(String contraseniaActual) {
        this.contraseniaActual = contraseniaActual;
    }

    public String getContraseniaNueva1() {
        return contraseniaNueva1;
    }
    public void setContraseniaNueva1(String contraseniaNueva1) {
        this.contraseniaNueva1 = contraseniaNueva1;
    }

    public String getContraseniaNueva2() {
        return contraseniaNueva2;
    }
    public void setContraseniaNueva2(String contraseniaNueva2) {
        this.contraseniaNueva2 = contraseniaNueva2;
    }
    
    public boolean isBanderaPanel1() {
        return banderaPanel1;
    }
    public void setBanderaPanel1(boolean banderaPanel1) {
        this.banderaPanel1 = banderaPanel1;
    }

    public boolean isBanderaPanel2() {
        return banderaPanel2;
    }
    public void setBanderaPanel2(boolean banderaPanel2) {
        this.banderaPanel2 = banderaPanel2;
    }

    public boolean isBanderaPanel3() {
        return banderaPanel3;
    }
    public void setBanderaPanel3(boolean banderaPanel3) {
        this.banderaPanel3 = banderaPanel3;
    }

    public boolean isBanderaPanel4() {
        return banderaPanel4;
    }
    public void setBanderaPanel4(boolean banderaPanel4) {
        this.banderaPanel4 = banderaPanel4;
    }
    
//****************************************************************************//
//                                   Métodos                                  //
//****************************************************************************//
    
    //Método para actualizar constraseña. (cambiar_contrasenia)  
    public void actualizarContrasenia() {
        try {
            if(contraseniaNueva2.equals(contraseniaNueva1)){
                usuarioSeleccionado = appSession.getUsuario();
                usuarioSeleccionado.setUsuarioContrasenia(contraseniaNueva1);
                getUsuariosFacade().edit(usuarioSeleccionado);
                contraseniaNueva1 = "";
                contraseniaNueva2 = "";
                banderaPanel1 = false;
                banderaPanel2 = false;
                banderaPanel3 = false;
                banderaPanel4 = true;
            }
            else{
                util.FacesUtil.addMensaje("Las contraseñas no coinciden.");
            }
            
        } catch (Exception e) {
            
        }
    }

    //Método para validar la contraseña actual. (cambiar_contrasenia) 
    public void validaContrasenia(){
        try{
            banderaPanel1 = true;
            banderaPanel2 = false;
            banderaPanel3 = false;
            banderaPanel4 = false;
            if (contraseniaActual.equals(appSession.getUsuario().getUsuarioContrasenia())) {
                banderaPanel1 = false;
                banderaPanel2 = true;
                banderaPanel3 = false;
                banderaPanel4 = false;
            }
            else{
                util.FacesUtil.addMensaje("La contraseña es incorrecta");
            }
        } catch (Exception e){
            
        }
    }
    
    //Método para validar la nueva contraseña. (cambiar_contrasenia) 
    public void nuevaContrasenia(){
        try{
            banderaPanel1 = false;
            banderaPanel2 = true;
            banderaPanel3 = false;
            banderaPanel3 = false;
            if (contraseniaNueva1.length() > 5 && contraseniaNueva1.length() < 21) {
                banderaPanel1 = false;
                banderaPanel2 = false;
                banderaPanel3 = true;
                banderaPanel4 = false;
            }
            else{
                util.FacesUtil.addMensaje("La contraseña debe tener entre 6 y 20 caracteres.");
            }
        } catch (Exception e){
            
        }
    }
    
    //Mpetodo para salir del sistema. (plantilla1)
    public void salirSistema(){
        appSession.setUsuario(null);
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest origRequest = (HttpServletRequest) context.getExternalContext().getRequest();
        String contextPath = origRequest.getContextPath();
        try{
            FacesContext.getCurrentInstance().getExternalContext().redirect(contextPath+"/login.jsf");
        }
        catch(IOException e){
            e.printStackTrace();
        }
    }
}
