package controladores;

import dao.MensajeFacade;
import entidades.Mensaje;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import org.primefaces.context.RequestContext;

//@author Juanfran Aldana
@Named(value = "mensajesBean")
@ViewScoped
public class MensajesBean implements Serializable{

    @EJB
    private MensajeFacade mensajeFacade;
    private Mensaje nuevoMensaje = new Mensaje();
    private Mensaje mensajeSeleccionado = new Mensaje();
    
    //Contructor de la clase.
    public MensajesBean() {
    }

    public List<Mensaje> todosMensajes(){
        return getMensajeFacade().findAll();}
    
    //Método Getter de la variable mensajeFacade.
    public MensajeFacade getMensajeFacade() {
        return mensajeFacade;
    }

    //Métodos Getter y Setter del objeto nuevoMensaje.
    public Mensaje getNuevoMensaje() {
        return nuevoMensaje;
    }
    public void setNuevoMensaje(Mensaje nuevoMensaje) {
        this.nuevoMensaje = nuevoMensaje;
    }
    
    //Método para guardar a la entidad Mensaje.
    public void guardarMensaje(){
        try {
            nuevoMensaje.setMensajeFecha(new Date());
            getMensajeFacade().create(nuevoMensaje);
            nuevoMensaje = new Mensaje();
            mensajeEnviado("Mensaje enviado satisfactoriamente.");
        } catch (Exception e) {
            mensajeNoEnviado("El mensaje no se envió.");
        }
    }
    
    //Método para mostrar confirmación de mensaje enviado.
    public void mensajeEnviado(String mensaje) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Mensaje", mensaje);
        RequestContext.getCurrentInstance().showMessageInDialog(message);
    }
    
    //Método para mostrar que no se logró enviar el mensaje.
    public void mensajeNoEnviado(String mensaje) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Mensaje", mensaje);
        RequestContext.getCurrentInstance().showMessageInDialog(message);
    }

    public Mensaje getMensajeSeleccionado() {
        return mensajeSeleccionado;
    }
    public void setMensajeSeleccionado(Mensaje mensajeSeleccionado) {
        this.mensajeSeleccionado = mensajeSeleccionado;
    }
      
}
