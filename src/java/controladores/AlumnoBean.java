package controladores;

import dao.AlumnoFacade;
import dao.BoletaFacade;
import dao.GradoFacade;
import dao.UsuariosFacade;
import entidades.Alumno;
import entidades.Boleta;
import entidades.Grado;
import entidades.Materia;
import entidades.Usuarios;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import org.primefaces.context.RequestContext;

//@author Juanfran Aldana
@ManagedBean
@SessionScoped
public class AlumnoBean implements Serializable {

    @EJB
    private UsuariosFacade usuariosFacade;
    private Usuarios usuarioSeleccionado = new Usuarios();
    
    @EJB
    private BoletaFacade boletaFacade;
    private Boleta nuevaBoleta = new Boleta();
    
    @EJB
    private GradoFacade gradoFacade;

    @EJB
    private AlumnoFacade alumnoFacade;
    private Alumno nuevoAlumno = new Alumno();
    private Alumno alumnoSeleccionado = new Alumno();

    //Para manejo de sesión.
    @ManagedProperty(value = "#{appSession}")
    private AppSession appSession;
    
    //Contructor de la clase.
    public AlumnoBean() {
    }

//****************************************************************************//
//                  Métodos que devuelven lista por entidades                 //
//****************************************************************************//

    public List<Grado> todosGrados(){
        return getGradoFacade().findAll();}
    
    //Método para obtener todos los alumnos
    public List<Alumno> todosAlumnos(){
        if(appSession.getUsuario().getRolId().getRolNombre().equalsIgnoreCase("Profesor")){
            return getAlumnoFacade().alumnosPorGrado(appSession.getUsuario().getProfesorId().getGradoId().getGradoId());
        }
        else{
            return getAlumnoFacade().findAll();
        }
    }

//****************************************************************************//
//                      Métodos GET y SET para variables                      //
//****************************************************************************//

    public BoletaFacade getBoletaFacade() {
        return boletaFacade;}

    public UsuariosFacade getUsuariosFacade() {
        return usuariosFacade;}
        
    public AlumnoFacade getAlumnoFacade() {
        return alumnoFacade;}

    public GradoFacade getGradoFacade() {
        return gradoFacade;}
    
    public Alumno getNuevoAlumno() {
        return nuevoAlumno;
    }
    public void setNuevoAlumno(Alumno nuevoAlumno) {
        this.nuevoAlumno = nuevoAlumno;
    }
    
    public Boleta getNuevaBoleta() {
        return nuevaBoleta;
    }
    public void setNuevaBoleta(Boleta nuevaBoleta) {
        this.nuevaBoleta = nuevaBoleta;
    }

    public Alumno getAlumnoSeleccionado() {
        return alumnoSeleccionado;
    }
    public void setAlumnoSeleccionado(Alumno alumnoSeleccionado) {
        this.alumnoSeleccionado = alumnoSeleccionado;
    }

    public Usuarios getUsuarioSeleccionado() {
        return usuarioSeleccionado;
    }
    public void setUsuarioSeleccionado(Usuarios usuarioSeleccionado) {
        this.usuarioSeleccionado = usuarioSeleccionado;
    }

    public AppSession getAppSession() {
        return appSession;
    }
    public void setAppSession(AppSession appSession) {
        this.appSession = appSession;
    }
    
//****************************************************************************//
//                                Otros métodos                               //
//****************************************************************************//
    
    //Método para guardar a la entidad Alumno.
    public void guardarAlumno(){
        try {
            getAlumnoFacade().create(nuevoAlumno);
            //Desde aqui comienza a crear la boleta del estudiante.
            for(int i=1; i<24; i++){
                nuevaBoleta.setAlumnoId(nuevoAlumno);
                nuevaBoleta.setMateriaId(new Materia(i));
                getBoletaFacade().create(nuevaBoleta);
                nuevaBoleta = new Boleta();
            }
            nuevoAlumno = new Alumno();
            mensajeEnviado("Datos guardados satisfactoriamente.");
        } catch (Exception e) {
            mensajeNoEnviado("Los datos no se guardaron.");
        }
    }
    
    public void actualizarAlumno(){
        try {
            getAlumnoFacade().edit(alumnoSeleccionado);
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("$('#editarAlumno').modal('hide');");
            mensajeEnviado("Datos actualizados satisfactoriamente.");
        } catch (Exception e) {
            mensajeNoEnviado("Los datos no se actualizaron.");
        }
    }
    
    //Método para eliminar en la entidad Alumno.
    public void eliminarAlumno(){
        try {
            usuarioSeleccionado = getUsuariosFacade().traeUsuarioPorAlumnoId(alumnoSeleccionado.getAlumnoId());
            getUsuariosFacade().remove(usuarioSeleccionado);
            getAlumnoFacade().remove(alumnoSeleccionado);
            usuarioSeleccionado = new Usuarios();
            alumnoSeleccionado = new Alumno();
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("$('#eliminarAlumno').modal('hide');");
            mensajeEnviado("Datos eliminados satisfactoriamente.");
        } catch (Exception e) {
            mensajeNoEnviado("Los datos no se eliminaron.");
        }
    }
    
    //Método para cerrar modal.
    public void cerrarModal(){
        try {
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("$('#eliminarAlumno').modal('hide');");
        } catch (Exception e) {
        }
    }
    
    //Método para mostrar confirmación de datos guardados.
    public void mensajeEnviado(String mensaje) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Mensaje", mensaje);
        RequestContext.getCurrentInstance().showMessageInDialog(message);
    }
    
    //Método para mostrar que no se logró guardar los datos.
    public void mensajeNoEnviado(String mensaje) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Mensaje", mensaje);
        RequestContext.getCurrentInstance().showMessageInDialog(message);
    }
    
}

