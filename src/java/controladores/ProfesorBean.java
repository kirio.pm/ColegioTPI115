/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import dao.GradoFacade;
import dao.ProfesorFacade;
import dao.UsuariosFacade;
import entidades.Grado;
import entidades.Profesor;
import entidades.Usuarios;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import org.primefaces.context.RequestContext;

/**
 *
 * @author kirio
 */
@ManagedBean
@SessionScoped
public class ProfesorBean implements Serializable {

    @EJB
    private UsuariosFacade usuariosFacade;
    private Usuarios usuarioSeleccionado = new Usuarios();
    
    @EJB
    private GradoFacade gradoFacade;

    @EJB
    private ProfesorFacade profesorFacade;
    private Profesor nuevoProfesor = new Profesor();
    private Profesor profesorSeleccionado = new Profesor();
    
    //Contructor de la clase.
    public ProfesorBean() {
    }

//****************************************************************************//
//                  Métodos que devuelven lista por entidades                 //
//****************************************************************************//

    public List<Grado> todosGrados(){
        return getGradoFacade().findAll();}
    
    public List<Profesor> todosProfesores(){
        return getProfesorFacade().findAll();}
    
//****************************************************************************//
//                      Métodos GET y SET para variables                      //
//****************************************************************************//

    public ProfesorFacade getProfesorFacade() {
        return profesorFacade;}

    public GradoFacade getGradoFacade() {
        return gradoFacade;}

    public UsuariosFacade getUsuariosFacade() {
        return usuariosFacade;
    }
    
    //Métodos Getter y Setter del objeto nuevoProfesor.
    public Profesor getNuevoProfesor() {
        return nuevoProfesor;}
    
    public void setNuevoProfesor(Profesor nuevoProfesor) {
        this.nuevoProfesor = nuevoProfesor;}

    //Métodos Getter y Setter del objeto profesorSeleccionado.
    public Profesor getProfesorSeleccionado() {
        return profesorSeleccionado;
    }
    public void setProfesorSeleccionado(Profesor profesorSeleccionado) {
        this.profesorSeleccionado = profesorSeleccionado;
    }

    public Usuarios getUsuarioSeleccionado() {
        return usuarioSeleccionado;
    }
    public void setUsuarioSeleccionado(Usuarios usuarioSeleccionado) {
        this.usuarioSeleccionado = usuarioSeleccionado;
    }
    
//****************************************************************************//
//                                Otros métodos                               //
//****************************************************************************//
    
    //Método para guardar a la entidad Profesor.
    public void guardarProfesor(){
        try {
            getProfesorFacade().create(nuevoProfesor);
            nuevoProfesor = new Profesor();
            mensajeEnviado("Datos guardados satisfactoriamente.");
        } catch (Exception e) {
            mensajeNoEnviado("Los datos no se guardaron.");
        }
    }
    
    //Método para actualizar en la entidad Profesor.
    public void actualizarProfesor(){
        try {
            getProfesorFacade().edit(profesorSeleccionado);
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("$('#editarProfesor').modal('hide');");
            mensajeEnviado("Datos actualizados satisfactoriamente.");
        } catch (Exception e) {
            mensajeNoEnviado("Los datos no se actualizaron.");
        }
    }
    
    //Método para eliminar en la entidad Profesor.
    public void eliminarProfesor(){
        try {
            usuarioSeleccionado = getUsuariosFacade().traeUsuarioPorProfesorId(profesorSeleccionado.getProfesorId());
            getUsuariosFacade().remove(usuarioSeleccionado);
            getProfesorFacade().remove(profesorSeleccionado);
            usuarioSeleccionado = new Usuarios();
            profesorSeleccionado = new Profesor();
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("$('#eliminarProfesor').modal('hide');");
            mensajeEnviado("Datos eliminados satisfactoriamente.");
        } catch (Exception e) {
            mensajeNoEnviado("Los datos no se eliminaron.");
        }
    }
    
    //Método para cerrar modal.
    public void cerrarModal(){
        try {
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("$('#eliminarProfesor').modal('hide');");
        } catch (Exception e) {
        }
    }
    
    //Método para mostrar confirmación de datos guardados/Actualizados.
    public void mensajeEnviado(String mensaje) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Mensaje", mensaje);
        RequestContext.getCurrentInstance().showMessageInDialog(message);
    }
    
    //Método para mostrar que no se logró guardar/Actualizar los datos.
    public void mensajeNoEnviado(String mensaje) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Mensaje", mensaje);
        RequestContext.getCurrentInstance().showMessageInDialog(message);
    }
    
}

