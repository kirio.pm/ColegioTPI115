
package controladores;
 
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;

//@author Juanfran Aldana
@ManagedBean

public class ImagenesBean {

    private List<String> images;
    @PostConstruct
    public void init() {
        images = new ArrayList<String>();
        for (int i = 1; i <= 6; i++) {
            images.add("imagen"+ i + ".jpg");
        }
    }
 
    public List<String> getImages() {
        return images;
    }
    
}
