package controladores;

import dao.UsuariosFacade;
import entidades.Usuarios;
import java.io.Serializable;
import util.FacesUtil;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.naming.NamingException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;

//@autor Juanfran Aldana
@ManagedBean
@ViewScoped
public class UsuarioBean  implements Serializable {
    
    @EJB
    private UsuariosFacade usuariosFacade;
    
//****************************************************************************//
//                                  Variables                                 //
//****************************************************************************//

    private String usuario;
    private String password;
  
    private String urlActual;
    private String usuarioSH;
    private List  roles;
   
    
    @ManagedProperty(value="#{appSession}")
    private AppSession appSession;

    public UsuarioBean() {
    }

//****************************************************************************//
//                                   Métodos                                  //
//****************************************************************************//

    //Método Get para obtener datos de entidad Usuarios
    public UsuariosFacade getUsuariosFacade() {
        return usuariosFacade;
    }
       
    //Método que define la acción por usuario logueado, direciona a página de inicio según usuario.
    public String logiAction() throws NamingException {
        String accion = null;
        try {
            System.out.println(usuario + password);
            Authentication request = new UsernamePasswordAuthenticationToken(usuario, password);
            System.out.println("Request: "+request);
            Authentication result = getAuthenticationManager().authenticate(request);
            System.out.println("Result: "+result);
            SecurityContextHolder.getContext().setAuthentication(result);
            String rol=postLogin();
            System.out.println("Rol: "+rol);
            if (rol!=null) {
                if (rol.equals("Director")) {
                    accion="index.xhtml?faces-redirect=true";
                }
                if (rol.equals("Profesor")) {
                    accion="index.xhtml?faces-redirect=true";
                }
                if (rol.equals("Estudiante")) {
                    accion="index.xhtml?faces-redirect=true";
                }
            }
        } catch (AuthenticationException e) {
            util.FacesUtil.addMensaje("Credenciales incorrectas, verifique...");
            e.printStackTrace();
        }
        return accion;
    }

    //Método que retorna el rol (perfil) del usuario logueado.
    private String postLogin() {
        
        String rol = null;
        
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String name = user.getUsername(); //obtenemos el usuario logueado    
       
        //Guardar el nombre de la session del usuario
        try{
            Usuarios usuario = getUsuariosFacade().traeUsuarioLogueado(name) ;
            System.out.println("Usuario autenticado: "+usuario.getUsuarioNombre()); 
            appSession.setUsuario(usuario);
        }catch(Exception e){
            System.out.println(e.toString());
        }
        if(appSession.getUsuario() !=null){
        rol=appSession.getUsuario().getRolId().getRolNombre();
        }
        
        return rol;
    }

    protected AuthenticationManager getAuthenticationManager() {
        return (AuthenticationManager) FacesUtil.getSpringBean("authenticationManager");
    }

//****************************************************************************//
//                      Métodos GET y SET para variables                      //
//****************************************************************************//
    
    public String getUsuario() {
        return usuario;
    }
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }
   public void setPassword(String password) {
        this.password = password;
    }

    public String getUrlActual() {
        return urlActual;
    }
    public void setUrlActual(String urlActual) {        
        this.urlActual = urlActual;
    }

    public String getUsuarioSH() {
        return usuarioSH;
    }
    public void setUsuarioSH(String usuarioSH) {
        this.usuarioSH = usuarioSH;
    }

    public List getRoles() {
        return roles;
    }
    public void setRoles(List roles) {
        this.roles = roles;
    }
    
    public AppSession getAppSession() {
        return appSession;
    }
    public void setAppSession(AppSession appSession) {
        this.appSession = appSession;
    }

}
