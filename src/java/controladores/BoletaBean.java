package controladores;

import dao.AlumnoFacade;
import dao.BoletaFacade;
import entidades.Alumno;
import entidades.Boleta;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

//@author Juanfran Aldana
@ManagedBean
@SessionScoped
public class BoletaBean implements Serializable {

    @EJB
    private AlumnoFacade alumnoFacade;
    private Alumno alumnoSeleccionado = new Alumno();
    private Alumno alumnoLegueado = new Alumno();
    
    @EJB
    private BoletaFacade boletaFacade;
    private Boleta boleta1 = new Boleta();
    private Boleta boleta2 = new Boleta();
    private Boleta boleta3 = new Boleta();
    private Boleta boleta4 = new Boleta();
    private Boleta boleta5 = new Boleta();
    private Boleta boleta6 = new Boleta();
    private Boleta boleta7 = new Boleta();
    private Boleta boleta8 = new Boleta();
    private Boleta boleta9 = new Boleta();
    private Boleta boleta10 = new Boleta();
    private Boleta boleta11 = new Boleta();
    private Boleta boleta12 = new Boleta();
    private Boleta boleta13 = new Boleta();
    private Boleta boleta14 = new Boleta();
    private Boleta boleta15 = new Boleta();
    private Boleta boleta16 = new Boleta();
    private Boleta boleta17 = new Boleta();
    private Boleta boleta18 = new Boleta();
    private Boleta boleta19 = new Boleta();
    private Boleta boleta20 = new Boleta();
    private Boleta boleta21 = new Boleta();
    private Boleta boleta22 = new Boleta();
    private Boleta boleta23 = new Boleta();
    
    private int alumno = 0;
    
    //Para manejo de sesión.
    @ManagedProperty(value = "#{appSession}")
    private AppSession appSession;
    
    //Contructor de la clase.
    public BoletaBean() {
    }

//****************************************************************************//
//                      Métodos GET y SET para variables                      //
//****************************************************************************//

    public AlumnoFacade getAlumnoFacade() {
        return alumnoFacade;}

    public BoletaFacade getBoletaFacade() {
        return boletaFacade;}

    public Alumno getAlumnoSeleccionado() {
        return alumnoSeleccionado;
    }
    public void setAlumnoSeleccionado(Alumno alumnoSeleccionado) {
        this.alumnoSeleccionado = alumnoSeleccionado;
    }

    public Alumno getAlumnoLegueado() {
        return alumnoLegueado;
    }
    public void setAlumnoLegueado(Alumno alumnoLegueado) {
        this.alumnoLegueado = alumnoLegueado;
    }
    
    public AppSession getAppSession() {
        return appSession;
    }
    public void setAppSession(AppSession appSession) {
        this.appSession = appSession;
    }

    public int getAlumno() {
        return alumno;
    }
    public void setAlumno(int alumno) {
        this.alumno = alumno;
    }
    //materia 1
    public Boleta getBoleta1() {
        return boleta1 = getBoletaFacade().traeBoleta(alumno, 1);
    }
    public void setBoleta1(Boleta boleta1) {
        this.boleta1 = boleta1;
    }
    //materia 2
    public Boleta getBoleta2() {
        return boleta2 = getBoletaFacade().traeBoleta(alumno, 2);
    }
    public void setBoleta2(Boleta boleta2) {
        this.boleta2 = boleta2;   
    }
    //materia 3
    public Boleta getBoleta3() {
        return boleta3 = getBoletaFacade().traeBoleta(alumno, 3);
    }
    public void setBoleta3(Boleta boleta3) {
        this.boleta3 = boleta3;   
    }  
    //materia 4
    public Boleta getBoleta4() {
        return boleta4 = getBoletaFacade().traeBoleta(alumno, 4);
    }
    public void setBoleta4(Boleta boleta4) {
        this.boleta4 = boleta4;   
    }
    //materia 5
    public Boleta getBoleta5() {
        return boleta5 = getBoletaFacade().traeBoleta(alumno, 5);
    }
    public void setBoleta5(Boleta boleta5) {
        this.boleta5 = boleta5;   
    }
    //materia 6
    public Boleta getBoleta6() {
        return boleta6 = getBoletaFacade().traeBoleta(alumno, 6);
    }
    public void setBoleta6(Boleta boleta6) {
        this.boleta6 = boleta6;   
    }
    //materia 7
    public Boleta getBoleta7() {
        return boleta7 = getBoletaFacade().traeBoleta(alumno, 7);
    }
    public void setBoleta7(Boleta boleta7) {
        this.boleta7 = boleta7;   
    }
    //materia 8
    public Boleta getBoleta8() {
        return boleta8 = getBoletaFacade().traeBoleta(alumno, 8);
    }
    public void setBoleta8(Boleta boleta8) {
        this.boleta8 = boleta8;   
    }
    //materia 9
    public Boleta getBoleta9() {
        return boleta9 = getBoletaFacade().traeBoleta(alumno, 9);
    }
    public void setBoleta9(Boleta boleta9) {
        this.boleta9 = boleta9;   
    }
    //materia 10
    public Boleta getBoleta10() {
        return boleta10 = getBoletaFacade().traeBoleta(alumno, 10);
    }
    public void setBoleta10(Boleta boleta10) {
        this.boleta10 = boleta10;   
    }
    //materia 11
    public Boleta getBoleta11() {
        return boleta11 = getBoletaFacade().traeBoleta(alumno, 11);
    }
    public void setBoleta11(Boleta boleta11) {
        this.boleta11 = boleta11;   
    }
    //materia 12
    public Boleta getBoleta12() {
        return boleta12 = getBoletaFacade().traeBoleta(alumno, 12);
    }
    public void setBoleta12(Boleta boleta12) {
        this.boleta12 = boleta12;   
    }
    //materia 13
    public Boleta getBoleta13() {
        return boleta13 = getBoletaFacade().traeBoleta(alumno, 13);
    }
    public void setBoleta13(Boleta boleta13) {
        this.boleta13 = boleta13;
    }
    //materia 14
     public Boleta getBoleta14() {
        return boleta14 = getBoletaFacade().traeBoleta(alumno, 14);
    }
    public void setBoleta14(Boleta boleta14) {
        this.boleta14 = boleta14;
    }
    //materia 15
     public Boleta getBoleta15() {
        return boleta15 = getBoletaFacade().traeBoleta(alumno, 15);
    }
    public void setBoleta15(Boleta boleta15) {
        this.boleta15 = boleta15;
    }
    //materia 16
     public Boleta getBoleta16() {
        return boleta16 = getBoletaFacade().traeBoleta(alumno, 16);
    }
    public void setBoleta16(Boleta boleta16) {
        this.boleta16 = boleta16;
    }
    //materia 17
     public Boleta getBoleta17() {
        return boleta17 = getBoletaFacade().traeBoleta(alumno, 17);
    }
    public void setBoleta17(Boleta boleta17) {
        this.boleta17 = boleta17;
    }
    //materia 18
     public Boleta getBoleta18() {
        return boleta18 = getBoletaFacade().traeBoleta(alumno, 18);
    }
    public void setBoleta18(Boleta boleta18) {
        this.boleta18 = boleta18;
    }
    //materia 19
     public Boleta getBoleta19() {
        return boleta19 = getBoletaFacade().traeBoleta(alumno, 19);
    }
    public void setBoleta19(Boleta boleta19) {
        this.boleta19 = boleta19;
    }
    //materia 20
     public Boleta getBoleta20() {
        return boleta20 = getBoletaFacade().traeBoleta(alumno, 20);
    }
    public void setBoleta20(Boleta boleta20) {
        this.boleta20 = boleta20;
    }
    //materia 21
     public Boleta getBoleta21() {
        return boleta21 = getBoletaFacade().traeBoleta(alumno, 21);
    }
    public void setBoleta21(Boleta boleta21) {
        this.boleta21 = boleta21;
    }
    //materia 22
     public Boleta getBoleta22() {
        return boleta22 = getBoletaFacade().traeBoleta(alumno, 22);
    }
    public void setBoleta22(Boleta boleta22) {
        this.boleta22 = boleta22;
    }
    //materia 23
     public Boleta getBoleta23() {
        return boleta23 = getBoletaFacade().traeBoleta(alumno, 23);
    }
    public void setBoleta23(Boleta boleta23) {
        this.boleta23 = boleta23;
    }
             
}

