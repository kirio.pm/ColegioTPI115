package controladores;

import dao.AlumnoFacade;
import dao.ProfesorFacade;
import dao.RolesFacade;
import dao.UsuariosFacade;
import entidades.Alumno;
import entidades.Profesor;
import entidades.Roles;
import entidades.Usuarios;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import org.primefaces.context.RequestContext;

//@autor Juanfran Aldana
@Named(value = "perfilesBean")
@ViewScoped
public class PerfilesBean implements Serializable{

    @EJB
    private RolesFacade rolesFacade;

    @EJB
    private UsuariosFacade usuariosFacade;
    private Usuarios nuevoUsuario = new Usuarios();
    private Usuarios usuarioSeleccionado = new Usuarios();
    
    @EJB
    private ProfesorFacade profesorFacade;

    @EJB
    private AlumnoFacade alumnoFacade;
        
    private int usuarioTipo = 0;
    
    //Contructor de la clase.
    public PerfilesBean() {
    }

//****************************************************************************//
//                  Métodos que devuelven lista por entidades                 //
//****************************************************************************//
    
    public List<Usuarios> todosUsuarios(){
        return getUsuariosFacade().findAll();
    }
    
    public List<Roles> todosRoles(){
        return getRolesFacade().rolesNoDirector();
    }
    
    public List<Alumno> todosAlumnos(){
        return getAlumnoFacade().findAll();
    }
    
    public List<Profesor> todosProfesores(){
        return getProfesorFacade().findAll();
    }
    
//****************************************************************************//
//                      Métodos GET y SET para variables                      //
//****************************************************************************//
    
    public RolesFacade getRolesFacade() {
        return rolesFacade;}

    public UsuariosFacade getUsuariosFacade() {
        return usuariosFacade;}

    public ProfesorFacade getProfesorFacade() {
        return profesorFacade;}

    public AlumnoFacade getAlumnoFacade() {
        return alumnoFacade;}

    public Usuarios getNuevoUsuario() {
        return nuevoUsuario;
    }
    public void setNuevoUsuario(Usuarios nuevoUsuario) {
        this.nuevoUsuario = nuevoUsuario;
    }

    public int getUsuarioTipo() {
        return usuarioTipo;
    }
    public void setUsuarioTipo(int usuarioTipo) {
        this.usuarioTipo = usuarioTipo;
    }

    public Usuarios getUsuarioSeleccionado() {
        return usuarioSeleccionado;
    }
    public void setUsuarioSeleccionado(Usuarios usuarioSeleccionado) {
        this.usuarioSeleccionado = usuarioSeleccionado;
    }

//****************************************************************************//
//                                Otros métodos                               //
//****************************************************************************//
    
    //Método para guardar a la entidad Usuarios.
    public void guardarUsuario(){
        try {
            if(usuarioTipo == 2){
                nuevoUsuario.setUsuarioUsuario(getProfesorFacade().profesorPorId(nuevoUsuario.getProfesorId().getProfesorId()).getProfesorCarnet());
                nuevoUsuario.setUsuarioNombre(getProfesorFacade().profesorPorId(nuevoUsuario.getProfesorId().getProfesorId()).getProfesorNombre() + " " + getProfesorFacade().profesorPorId(nuevoUsuario.getProfesorId().getProfesorId()).getProfesorApellido());
            }
            if(usuarioTipo == 3){
                nuevoUsuario.setUsuarioUsuario(getAlumnoFacade().alumnoPorId(nuevoUsuario.getAlumnoId().getAlumnoId()).getAlumnoCarnet());
                nuevoUsuario.setUsuarioNombre(getAlumnoFacade().alumnoPorId(nuevoUsuario.getAlumnoId().getAlumnoId()).getAlumnoNombre() + " " + getAlumnoFacade().alumnoPorId(nuevoUsuario.getAlumnoId().getAlumnoId()).getAlumnoApellido());
            }
            nuevoUsuario.setUsuarioEstado(Boolean.TRUE);
            nuevoUsuario.setRolId(new Roles(usuarioTipo));
            getUsuariosFacade().create(nuevoUsuario);
            usuarioTipo = 0;
            nuevoUsuario = new Usuarios();
            mensajeEnviado("Datos guardados satisfactoriamente.");
        } catch (Exception e) {
            mensajeNoEnviado("El usuario ya existe.");
        }
    }
    
    //Método para actualizar en la entidad Usuarios.
    public void actualizarUsuario(){
        try {
            getUsuariosFacade().edit(usuarioSeleccionado);
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("$('#editarUsuario').modal('hide');");
            mensajeEnviado("Datos actualizados satisfactoriamente.");
        } catch (Exception e) {
            mensajeNoEnviado("Los datos no se actualizaron.");
        }
    }
    
    //Método para eliminar en la entidad Usuarios.
    public void eliminarUsuario(){
        try {
            getUsuariosFacade().remove(usuarioSeleccionado);
            usuarioSeleccionado = new Usuarios();
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("$('#eliminarUsuario').modal('hide');");
            mensajeEnviado("Datos eliminados satisfactoriamente.");
        } catch (Exception e) {
            mensajeNoEnviado("Los datos no se eliminaron.");
        }
    }
    
    //Método para cerrar modal.
    public void cerrarModal(){
        try {
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("$('#eliminarUsuario').modal('hide');");
        } catch (Exception e) {
        }
    }
    
    //Método para mostrar confirmación de datos guardados.
    public void mensajeEnviado(String mensaje) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Mensaje", mensaje);
        RequestContext.getCurrentInstance().showMessageInDialog(message);
    }
    
    //Método para mostrar que no se logró guardar los datos.
    public void mensajeNoEnviado(String mensaje) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Mensaje", mensaje);
        RequestContext.getCurrentInstance().showMessageInDialog(message);
    }
    
}
