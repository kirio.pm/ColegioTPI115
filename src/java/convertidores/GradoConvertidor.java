package convertidores;

import dao.GradoFacade;
import entidades.Grado;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.inject.Named;

//@autor Juanfran Aldana
@Named(value = "gradoConvertidor")
@ManagedBean
@ViewScoped
public class GradoConvertidor implements Converter{
    
    @EJB
    private GradoFacade gradoFacade;

    public GradoConvertidor() {
    }

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (value.trim().equals("") || value.trim().equals("Seleccione uno...")) {
            return null;
        } else {
            try {
                int id = Integer.parseInt(value);
                Grado grad = getGradoFacade().find(id);
                return grad;
            } catch (Exception e) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error de conversión", "Seleccione un grado."));
            }
        }
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (!(value instanceof Grado)) {
            return null;
        }
        return String.valueOf(((Grado) value).getGradoId());
    }

    public GradoFacade getGradoFacade() {
        return gradoFacade;
    }
    
}
