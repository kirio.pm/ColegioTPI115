package convertidores;

import dao.ProfesorFacade;
import entidades.Profesor;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.inject.Named;

//@autor Juanfran Aldana
@Named(value = "profesorConvertidor")
@ManagedBean
@ViewScoped
public class ProfesorConvertidor implements Converter{
    
    @EJB
    private ProfesorFacade profesorFacade;

    public ProfesorConvertidor() {
    }

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (value.trim().equals("") || value.trim().equals("Seleccione uno...")) {
            return null;
        } else {
            try {
                int id = Integer.parseInt(value);
                Profesor pro = getProfesorFacade().find(id);
                return pro;
            } catch (Exception e) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error de conversión", "Seleccione un profesor."));
            }
        }
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (!(value instanceof Profesor)) {
            return null;
        }
        return String.valueOf(((Profesor) value).getProfesorId());
    }

    public ProfesorFacade getProfesorFacade() {
        return profesorFacade;
    }
    
}
