package convertidores;

import dao.AlumnoFacade;
import entidades.Alumno;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.inject.Named;

//@autor Juanfran Aldana
@Named(value = "alumnoConvertidor")
@ManagedBean
@ViewScoped
public class AlumnoConvertidor implements Converter{
    
    @EJB
    private AlumnoFacade alumnoFacade;

    public AlumnoConvertidor() {
    }

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (value.trim().equals("") || value.trim().equals("Seleccione uno...")) {
            return null;
        } else {
            try {
                int id = Integer.parseInt(value);
                Alumno alu = getAlumnoFacade().find(id);
                return alu;
            } catch (Exception e) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error de conversión", "Seleccione un alumno."));
            }
        }
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (!(value instanceof Alumno)) {
            return null;
        }
        return String.valueOf(((Alumno) value).getAlumnoId());
    }

    public AlumnoFacade getAlumnoFacade() {
        return alumnoFacade;
    }
    
}
