package entidades;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

//@autor Juanfran Aldana
@Entity
@Table(name = "alumno")
@NamedQueries({
//**/@NamedQuery(name = "Alumno.AlumnoPorCarnet", query = "SELECT a FROM Alumno a WHERE a.alumnoCarnet = :carnet"), //(nuevo_usuario)
/**/@NamedQuery(name = "Alumno.AlumnosPorGrado", query = "SELECT a FROM Alumno a WHERE a.gradoId.gradoId = :grado"), //(ver_alumnos)
    @NamedQuery(name = "Alumno.findAll", query = "SELECT a FROM Alumno a"),
    @NamedQuery(name = "Alumno.findByAlumnoId", query = "SELECT a FROM Alumno a WHERE a.alumnoId = :alumnoId"),
    @NamedQuery(name = "Alumno.findByAlumnoNombre", query = "SELECT a FROM Alumno a WHERE a.alumnoNombre = :alumnoNombre"),
    @NamedQuery(name = "Alumno.findByAlumnoApellido", query = "SELECT a FROM Alumno a WHERE a.alumnoApellido = :alumnoApellido"),
    @NamedQuery(name = "Alumno.findByAlumnoDireccion", query = "SELECT a FROM Alumno a WHERE a.alumnoDireccion = :alumnoDireccion"),
    @NamedQuery(name = "Alumno.findByAlumnoTelefono", query = "SELECT a FROM Alumno a WHERE a.alumnoTelefono = :alumnoTelefono"),
    @NamedQuery(name = "Alumno.findByAlumnoFecha", query = "SELECT a FROM Alumno a WHERE a.alumnoFecha = :alumnoFecha"),
    @NamedQuery(name = "Alumno.findByAlumnoCarnet", query = "SELECT a FROM Alumno a WHERE a.alumnoCarnet = :alumnoCarnet"),
    @NamedQuery(name = "Alumno.findByAlumnoNie", query = "SELECT a FROM Alumno a WHERE a.alumnoNie = :alumnoNie")})
public class Alumno implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "alumno_id")
    private Integer alumnoId;
    @Size(max = 75)
    @Column(name = "alumno_nombre")
    private String alumnoNombre;
    @Size(max = 75)
    @Column(name = "alumno_apellido")
    private String alumnoApellido;
    @Size(max = 250)
    @Column(name = "alumno_direccion")
    private String alumnoDireccion;
    @Size(max = 10)
    @Column(name = "alumno_telefono")
    private String alumnoTelefono;
    @Column(name = "alumno_fecha")
    @Temporal(TemporalType.DATE)
    private Date alumnoFecha;
    @Size(max = 10)
    @Column(name = "alumno_carnet")
    private String alumnoCarnet;
    @Column(name = "alumno_nie")
    private Long alumnoNie;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "alumnoId")
    private List<Boleta> boletaList;
    @JoinColumn(name = "grado_id", referencedColumnName = "grado_id")
    @ManyToOne
    private Grado gradoId;
    @OneToMany(mappedBy = "alumnoId")
    private List<Usuarios> usuariosList;

    public Alumno() {
    }

    public Alumno(Integer alumnoId) {
        this.alumnoId = alumnoId;
    }

    public Integer getAlumnoId() {
        return alumnoId;
    }

    public void setAlumnoId(Integer alumnoId) {
        this.alumnoId = alumnoId;
    }

    public String getAlumnoNombre() {
        return alumnoNombre;
    }

    public void setAlumnoNombre(String alumnoNombre) {
        this.alumnoNombre = alumnoNombre;
    }

    public String getAlumnoApellido() {
        return alumnoApellido;
    }

    public void setAlumnoApellido(String alumnoApellido) {
        this.alumnoApellido = alumnoApellido;
    }

    public String getAlumnoDireccion() {
        return alumnoDireccion;
    }

    public void setAlumnoDireccion(String alumnoDireccion) {
        this.alumnoDireccion = alumnoDireccion;
    }

    public String getAlumnoTelefono() {
        return alumnoTelefono;
    }

    public void setAlumnoTelefono(String alumnoTelefono) {
        this.alumnoTelefono = alumnoTelefono;
    }

    public Date getAlumnoFecha() {
        return alumnoFecha;
    }

    public void setAlumnoFecha(Date alumnoFecha) {
        this.alumnoFecha = alumnoFecha;
    }

    public String getAlumnoCarnet() {
        return alumnoCarnet;
    }

    public void setAlumnoCarnet(String alumnoCarnet) {
        this.alumnoCarnet = alumnoCarnet;
    }

    public Long getAlumnoNie() {
        return alumnoNie;
    }

    public void setAlumnoNie(Long alumnoNie) {
        this.alumnoNie = alumnoNie;
    }

    public List<Boleta> getBoletaList() {
        return boletaList;
    }

    public void setBoletaList(List<Boleta> boletaList) {
        this.boletaList = boletaList;
    }

    public Grado getGradoId() {
        return gradoId;
    }

    public void setGradoId(Grado gradoId) {
        this.gradoId = gradoId;
    }

    public List<Usuarios> getUsuariosList() {
        return usuariosList;
    }

    public void setUsuariosList(List<Usuarios> usuariosList) {
        this.usuariosList = usuariosList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (alumnoId != null ? alumnoId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Alumno)) {
            return false;
        }
        Alumno other = (Alumno) object;
        if ((this.alumnoId == null && other.alumnoId != null) || (this.alumnoId != null && !this.alumnoId.equals(other.alumnoId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades.Alumno[ alumnoId=" + alumnoId + " ]";
    }
    
}
