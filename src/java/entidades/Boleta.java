package entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

//@autor Juanfran Aldana
@Entity
@Table(name = "boleta")
@NamedQueries({
/**/@NamedQuery(name = "Boleta.Notas", query = "SELECT b FROM Boleta b WHERE b.alumnoId.alumnoId = :alumnoId AND b.materiaId.materiaId = :materiaId"),
    @NamedQuery(name = "Boleta.findAll", query = "SELECT b FROM Boleta b"),
    @NamedQuery(name = "Boleta.findByBoletaId", query = "SELECT b FROM Boleta b WHERE b.boletaId = :boletaId"),
    @NamedQuery(name = "Boleta.findByNota1", query = "SELECT b FROM Boleta b WHERE b.nota1 = :nota1"),
    @NamedQuery(name = "Boleta.findByNota2", query = "SELECT b FROM Boleta b WHERE b.nota2 = :nota2"),
    @NamedQuery(name = "Boleta.findByNota3", query = "SELECT b FROM Boleta b WHERE b.nota3 = :nota3"),
    @NamedQuery(name = "Boleta.findByNota4", query = "SELECT b FROM Boleta b WHERE b.nota4 = :nota4"),
    @NamedQuery(name = "Boleta.findByNota5", query = "SELECT b FROM Boleta b WHERE b.nota5 = :nota5"),
    @NamedQuery(name = "Boleta.findByNota6", query = "SELECT b FROM Boleta b WHERE b.nota6 = :nota6"),
    @NamedQuery(name = "Boleta.findByNota7", query = "SELECT b FROM Boleta b WHERE b.nota7 = :nota7"),
    @NamedQuery(name = "Boleta.findByNota8", query = "SELECT b FROM Boleta b WHERE b.nota8 = :nota8"),
    @NamedQuery(name = "Boleta.findByNota9", query = "SELECT b FROM Boleta b WHERE b.nota9 = :nota9")})
public class Boleta implements Serializable {

    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "nota1")
private Double nota1;
    @Column(name = "nota2")
    private Double nota2;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "nota3")
    private Double nota3;
    @Column(name = "nota4")
    private Double nota4;
    @Column(name = "nota5")
    private Double nota5;
    @Column(name = "nota6")
    private Double nota6;
    @Column(name = "nota7")
    private Double nota7;
    @Column(name = "nota8")
    private Double nota8;
    @Column(name = "nota9")
    private Double nota9;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "boleta_id")
    private Integer boletaId;
    @JoinColumn(name = "materia_id", referencedColumnName = "materia_id")
    @ManyToOne(optional = false)
    private Materia materiaId;
    @JoinColumn(name = "alumno_id", referencedColumnName = "alumno_id")
    @ManyToOne(optional = false)
    private Alumno alumnoId;

    public Boleta() {
    }

    public Boleta(Integer boletaId) {
        this.boletaId = boletaId;
    }

    public Integer getBoletaId() {
        return boletaId;
    }

    public void setBoletaId(Integer boletaId) {
        this.boletaId = boletaId;
    }


    public Materia getMateriaId() {
        return materiaId;
    }

    public void setMateriaId(Materia materiaId) {
        this.materiaId = materiaId;
    }

    public Alumno getAlumnoId() {
        return alumnoId;
    }

    public void setAlumnoId(Alumno alumnoId) {
        this.alumnoId = alumnoId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (boletaId != null ? boletaId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Boleta)) {
            return false;
        }
        Boleta other = (Boleta) object;
        if ((this.boletaId == null && other.boletaId != null) || (this.boletaId != null && !this.boletaId.equals(other.boletaId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades.Boleta[ boletaId=" + boletaId + " ]";
    }

    public Double getNota1() {
        return nota1;
    }

    public void setNota1(Double nota1) {
        this.nota1 = nota1;
    }

    public Double getNota2() {
        return nota2;
    }

    public void setNota2(Double nota2) {
        this.nota2 = nota2;
    }

    public Double getNota3() {
        return nota3;
    }

    public void setNota3(Double nota3) {
        this.nota3 = nota3;
    }

    public Double getNota4() {
        return nota4;
    }

    public void setNota4(Double nota4) {
        this.nota4 = nota4;
    }

    public Double getNota5() {
        return nota5;
    }

    public void setNota5(Double nota5) {
        this.nota5 = nota5;
    }

    public Double getNota6() {
        return nota6;
    }

    public void setNota6(Double nota6) {
        this.nota6 = nota6;
    }

    public Double getNota7() {
        return nota7;
    }

    public void setNota7(Double nota7) {
        this.nota7 = nota7;
    }

    public Double getNota8() {
        return nota8;
    }

    public void setNota8(Double nota8) {
        this.nota8 = nota8;
    }

    public Double getNota9() {
        return nota9;
    }

    public void setNota9(Double nota9) {
        this.nota9 = nota9;
    }
    
}
