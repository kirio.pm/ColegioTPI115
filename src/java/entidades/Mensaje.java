package entidades;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

//@autor Juanfran Aldana
@Entity
@Table(name = "mensaje")
@NamedQueries({
    @NamedQuery(name = "Mensaje.findAll", query = "SELECT m FROM Mensaje m"),
    @NamedQuery(name = "Mensaje.findByMensajeId", query = "SELECT m FROM Mensaje m WHERE m.mensajeId = :mensajeId"),
    @NamedQuery(name = "Mensaje.findByMensajeNombre", query = "SELECT m FROM Mensaje m WHERE m.mensajeNombre = :mensajeNombre"),
    @NamedQuery(name = "Mensaje.findByMensajeCorreo", query = "SELECT m FROM Mensaje m WHERE m.mensajeCorreo = :mensajeCorreo"),
    @NamedQuery(name = "Mensaje.findByMensajeMensaje", query = "SELECT m FROM Mensaje m WHERE m.mensajeMensaje = :mensajeMensaje"),
    @NamedQuery(name = "Mensaje.findByMensajeFecha", query = "SELECT m FROM Mensaje m WHERE m.mensajeFecha = :mensajeFecha")})
public class Mensaje implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "mensaje_id")
    private Integer mensajeId;
    @Size(max = 75)
    @Column(name = "mensaje_nombre")
    private String mensajeNombre;
    @Size(max = 50)
    @Column(name = "mensaje_correo")
    private String mensajeCorreo;
    @Size(max = 500)
    @Column(name = "mensaje_mensaje")
    private String mensajeMensaje;
    @Column(name = "mensaje_fecha")
    @Temporal(TemporalType.DATE)
    private Date mensajeFecha;

    public Mensaje() {
    }

    public Mensaje(Integer mensajeId) {
        this.mensajeId = mensajeId;
    }

    public Integer getMensajeId() {
        return mensajeId;
    }

    public void setMensajeId(Integer mensajeId) {
        this.mensajeId = mensajeId;
    }

    public String getMensajeNombre() {
        return mensajeNombre;
    }

    public void setMensajeNombre(String mensajeNombre) {
        this.mensajeNombre = mensajeNombre;
    }

    public String getMensajeCorreo() {
        return mensajeCorreo;
    }

    public void setMensajeCorreo(String mensajeCorreo) {
        this.mensajeCorreo = mensajeCorreo;
    }

    public String getMensajeMensaje() {
        return mensajeMensaje;
    }

    public void setMensajeMensaje(String mensajeMensaje) {
        this.mensajeMensaje = mensajeMensaje;
    }

    public Date getMensajeFecha() {
        return mensajeFecha;
    }

    public void setMensajeFecha(Date mensajeFecha) {
        this.mensajeFecha = mensajeFecha;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (mensajeId != null ? mensajeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Mensaje)) {
            return false;
        }
        Mensaje other = (Mensaje) object;
        if ((this.mensajeId == null && other.mensajeId != null) || (this.mensajeId != null && !this.mensajeId.equals(other.mensajeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades.Mensaje[ mensajeId=" + mensajeId + " ]";
    }
    
}
