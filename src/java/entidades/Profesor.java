package entidades;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

//@autor Juanfran Aldana
@Entity
@Table(name = "profesor")
@NamedQueries({
//**/@NamedQuery(name = "Profesor.ProfesorPorId", query = "SELECT p FROM Profesor p WHERE p.profesorId = :profesorId"), //(nuevo_usuario)
    @NamedQuery(name = "Profesor.findAll", query = "SELECT p FROM Profesor p"),
    @NamedQuery(name = "Profesor.findByProfesorId", query = "SELECT p FROM Profesor p WHERE p.profesorId = :profesorId"),
    @NamedQuery(name = "Profesor.findByProfesorNombre", query = "SELECT p FROM Profesor p WHERE p.profesorNombre = :profesorNombre"),
    @NamedQuery(name = "Profesor.findByProfesorApellido", query = "SELECT p FROM Profesor p WHERE p.profesorApellido = :profesorApellido"),
    @NamedQuery(name = "Profesor.findByProfesorDireccion", query = "SELECT p FROM Profesor p WHERE p.profesorDireccion = :profesorDireccion"),
    @NamedQuery(name = "Profesor.findByProfesorTelefono", query = "SELECT p FROM Profesor p WHERE p.profesorTelefono = :profesorTelefono"),
    @NamedQuery(name = "Profesor.findByProfesorFecha", query = "SELECT p FROM Profesor p WHERE p.profesorFecha = :profesorFecha"),
    @NamedQuery(name = "Profesor.findByProfesorCarnet", query = "SELECT p FROM Profesor p WHERE p.profesorCarnet = :profesorCarnet")})
public class Profesor implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "profesor_id")
    private Integer profesorId;
    @Size(max = 75)
    @Column(name = "profesor_nombre")
    private String profesorNombre;
    @Size(max = 75)
    @Column(name = "profesor_apellido")
    private String profesorApellido;
    @Size(max = 250)
    @Column(name = "profesor_direccion")
    private String profesorDireccion;
    @Size(max = 10)
    @Column(name = "profesor_telefono")
    private String profesorTelefono;
    @Column(name = "profesor_fecha")
    @Temporal(TemporalType.DATE)
    private Date profesorFecha;
    @Size(max = 10)
    @Column(name = "profesor_carnet")
    private String profesorCarnet;
    @JoinColumn(name = "grado_id", referencedColumnName = "grado_id")
    @ManyToOne(optional = false)
    private Grado gradoId;
    @OneToMany(mappedBy = "profesorId")
    private List<Usuarios> usuariosList;

    public Profesor() {
    }

    public Profesor(Integer profesorId) {
        this.profesorId = profesorId;
    }

    public Integer getProfesorId() {
        return profesorId;
    }

    public void setProfesorId(Integer profesorId) {
        this.profesorId = profesorId;
    }

    public String getProfesorNombre() {
        return profesorNombre;
    }

    public void setProfesorNombre(String profesorNombre) {
        this.profesorNombre = profesorNombre;
    }

    public String getProfesorApellido() {
        return profesorApellido;
    }

    public void setProfesorApellido(String profesorApellido) {
        this.profesorApellido = profesorApellido;
    }

    public String getProfesorDireccion() {
        return profesorDireccion;
    }

    public void setProfesorDireccion(String profesorDireccion) {
        this.profesorDireccion = profesorDireccion;
    }

    public String getProfesorTelefono() {
        return profesorTelefono;
    }

    public void setProfesorTelefono(String profesorTelefono) {
        this.profesorTelefono = profesorTelefono;
    }

    public Date getProfesorFecha() {
        return profesorFecha;
    }

    public void setProfesorFecha(Date profesorFecha) {
        this.profesorFecha = profesorFecha;
    }

    public String getProfesorCarnet() {
        return profesorCarnet;
    }

    public void setProfesorCarnet(String profesorCarnet) {
        this.profesorCarnet = profesorCarnet;
    }

    public Grado getGradoId() {
        return gradoId;
    }

    public void setGradoId(Grado gradoId) {
        this.gradoId = gradoId;
    }

    public List<Usuarios> getUsuariosList() {
        return usuariosList;
    }

    public void setUsuariosList(List<Usuarios> usuariosList) {
        this.usuariosList = usuariosList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (profesorId != null ? profesorId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Profesor)) {
            return false;
        }
        Profesor other = (Profesor) object;
        if ((this.profesorId == null && other.profesorId != null) || (this.profesorId != null && !this.profesorId.equals(other.profesorId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades.Profesor[ profesorId=" + profesorId + " ]";
    }
    
}
