package entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

//@autor Juanfran Aldana
@Entity
@Table(name = "grado")
@NamedQueries({
    @NamedQuery(name = "Grado.findAll", query = "SELECT g FROM Grado g"),
    @NamedQuery(name = "Grado.findByGradoId", query = "SELECT g FROM Grado g WHERE g.gradoId = :gradoId"),
    @NamedQuery(name = "Grado.findByGradoNombre", query = "SELECT g FROM Grado g WHERE g.gradoNombre = :gradoNombre"),
    @NamedQuery(name = "Grado.findByGradoDescripcion", query = "SELECT g FROM Grado g WHERE g.gradoDescripcion = :gradoDescripcion")})
public class Grado implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "grado_id")
    private Integer gradoId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "grado_nombre")
    private String gradoNombre;
    @Size(max = 250)
    @Column(name = "grado_descripcion")
    private String gradoDescripcion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "gradoId")
    private List<Profesor> profesorList;
    @OneToMany(mappedBy = "gradoId")
    private List<Alumno> alumnoList;

    public Grado() {
    }

    public Grado(Integer gradoId) {
        this.gradoId = gradoId;
    }

    public Grado(Integer gradoId, String gradoNombre) {
        this.gradoId = gradoId;
        this.gradoNombre = gradoNombre;
    }

    public Integer getGradoId() {
        return gradoId;
    }

    public void setGradoId(Integer gradoId) {
        this.gradoId = gradoId;
    }

    public String getGradoNombre() {
        return gradoNombre;
    }

    public void setGradoNombre(String gradoNombre) {
        this.gradoNombre = gradoNombre;
    }

    public String getGradoDescripcion() {
        return gradoDescripcion;
    }

    public void setGradoDescripcion(String gradoDescripcion) {
        this.gradoDescripcion = gradoDescripcion;
    }

    public List<Profesor> getProfesorList() {
        return profesorList;
    }

    public void setProfesorList(List<Profesor> profesorList) {
        this.profesorList = profesorList;
    }

    public List<Alumno> getAlumnoList() {
        return alumnoList;
    }

    public void setAlumnoList(List<Alumno> alumnoList) {
        this.alumnoList = alumnoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gradoId != null ? gradoId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Grado)) {
            return false;
        }
        Grado other = (Grado) object;
        if ((this.gradoId == null && other.gradoId != null) || (this.gradoId != null && !this.gradoId.equals(other.gradoId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades.Grado[ gradoId=" + gradoId + " ]";
    }
    
}
